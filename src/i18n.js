export default {
    "ca": {
        "messages": {
            "achievement": "Demostra haver assolit els coneixements i les" +
                " competències",
            "print": "Imprimir",
            "printTitle": "Vista d'impressió"
        },
        "fields": {
            "id": "Identificador universal",
            "name": "En/Na",
            "birthplace": "Nascut/da a",
            "nationality": "de nacionalitat",
            "nationalId": "i amb passaport o document nacional d'identitat",
            "registryCode": "Registre oficial de títols"
        },
        "nationalities": {
            "AD": "Andorrana",
            "ES": "Espanyola",
            "FR": "Francesa"
        },
        "jobs": {
            "RECTOR": "El rector",
            "MANAGER": "El gestor",
            "MINISTER": "La ministra"
        }
    }
}
