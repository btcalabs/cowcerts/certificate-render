import {html, LitElement} from "lit-element";
import {render} from "lit-html";
import moment from 'moment/src/moment';
import 'moment/src/locale/ca';
import '@zxing/library/umd/index.min';


import i18n from "./i18n.js";
import territories from "./territories.js";
import CSS from './cowcert-css';
import '@polymer/font-roboto';
import printCSS from './cowcert-print-css';

class Cowcert extends LitElement {

    constructor(language = 'ca') {
        super();
        this.language = language;
        this.setLocale();
        this.updateComplete.then(() => {
            this.afterRender()
        });
    }

    setLocale() {
        moment.locale(this.language);
    }

    getEndorsements() {
        if (this.certificate.signature instanceof Object &&
            this.certificate.signature.endorsements instanceof Array)
            return this.certificate.signature.endorsements;
        else
            return null;
    }

    getEndorsementByClaimType(claimType) {
        let endorsements = this.getEndorsements();
        if (!endorsements) return null;

        let foundEndorsement = null;

        endorsements.forEach((endorsement) => {
            if (!endorsement.claim instanceof Object)
                return;

            let claim = endorsement.claim;

            if (claim.type instanceof Array) {
                claim.type.forEach((type) => {
                    if (type.includes(claimType)) {
                        foundEndorsement = endorsement;
                    }
                });
            }
        });

        return foundEndorsement;
    }

    getRecipient() {
        return this.getEndorsementByClaimType('RecipientClaim')
    }

    getRecipientName() {
        let recipientEndorsement = this.getRecipient();
        if (recipientEndorsement) {
            return `${recipientEndorsement.claim.givenName} ${recipientEndorsement.claim.familyName}`;
        } else if (this.certificate && this.certificate.recipientProfile && this.certificate.recipientProfile.name) {
            return this.certificate.recipientProfile.name;
        }
        return null
    }


    getFormalization() {
        return this.certificate.officialValidation || null;
    }

    translate(section, item) {

        const lang = this.language;
        const translationQuery = `["${lang}"]["${section}"]["${item}"]`;

        // Not enough arguments
        if (!lang || !section || !item)
            return `TranslateCallError(${translationQuery})`;

        // Language not present
        let languageTranslations = Cowcert.translations[lang];
        if (!languageTranslations)
            return translationQuery;

        // Section not present
        let sectionTranslations = languageTranslations[section];
        if (!sectionTranslations)
            return translationQuery;

        // Item not present
        let translation = sectionTranslations[item];
        if (!translation)
            return translationQuery;

        return translation;
    }

    translateField(field) {
        return this.translate('fields', field);
    }

    translateNationality(nationality) {
        return territories.main["ca-AD"]["localeDisplayNames"]["territories"][nationality];
    }

    translateMessage(message) {
        return this.translate('messages', message)
    }

    translateJob(job) {
        return this.translate('jobs', job)
    }

    translateDate(date) {
        return moment(date).format('LL');
    }

    createQRElement(size) {
        let qr = new ZXing.BrowserQRCodeSvgWriter();
        let element = qr.write(this.certificate.id, size, size);
        element.id = "qr";
        return element;
    }

    render() {
        if (this.certificate) {

            let recipientEndorsement = this.getRecipient();
            let recipientClaim;
            if (recipientEndorsement)
                recipientClaim = recipientEndorsement.claim;

            let formalizationEndorsement = this.getFormalization();
            let formalizationClaim;
            if (formalizationEndorsement)
                formalizationClaim = formalizationEndorsement.claim;

            let badge = this.certificate.badge;

            let headHTML = html`
                <header>
                    <h1>${badge.name}</h1>
                    <h2>${badge.description}</h2>
                </header>
            `

            let legalHTML = badge.legalText ?
                html`<p class="legal">${badge.legalText}</p>` : html``;

            let infoHTML = html`
                <dl>
                   <dt>${this.translateField('name')}</dt> 
                   <dd class="name">${this.getRecipientName()}</dd>
                   <dt>${this.translateField('birthplace')}</dt>
                   <dd>${recipientClaim.birthplace}</dd>
                   <dt>${this.translateField('nationality')}</dt>
                   <dd>${this.translateNationality(recipientClaim.nationality)}</dd>
                   <dt>${this.translateField('nationalId')}</dt>
                   <dd>${recipientClaim.nationalId}</dd>
                </dl>    
                `;

            let achievementHTML = html`
                <p class="achievement">${this.translateMessage('achievement')}</p>
            `;

            let issuer = badge.issuer;
            let issuerManager = this.certificate.signatureLines[0];
            let ministryIssuer = formalizationEndorsement ? formalizationEndorsement.issuer : null;
            let ministryIssuerManager = ministryIssuer ? formalizationClaim.ministrySignature : null;
            let signaturesHTML = html`
                <div class="signatures">
                    <div class="issuer">
                        <p class="name">${issuer.name}</p>
                        <img class="logo" src="${issuer.image}"> 
                        <p class="job">${this.translateJob(issuerManager.jobTitle)}</p>
                        <img class="signature" src="${issuerManager.image}">
                        <p class="manager">${issuerManager.name}</p>
                    </div>
                    ${formalizationClaim ? html`
                    <div class="ministry">
                        <p class="name">${ministryIssuer.name}</p>
                        <img class="logo" src="${ministryIssuer.image}"> 
                        <p class="job">${this.translateJob(ministryIssuerManager.jobTitle)}</p>
                        <img class="signature" src="${ministryIssuerManager.image}">
                        <p class="manager">${ministryIssuerManager.name}</p>
                    </div>` : html``}
                </div>
            `;

            let footerHTML = html`
                <footer>
                   ${this.translateDate(this.certificate.issuedOn)}<br>
                   ${this.translateField('registryCode')}: ${formalizationClaim.registryCode}<br>
                   ${this.translateField('id')}: <code>${this.certificate.universalIdentifier}</code>
                </footer>
            `;

            let printButtonHTML = this.noprint ? html`` : html`
                <paper-button raised id="print" @click="${this.print}">
                    <iron-icon icon="print"></iron-icon>&nbsp;
                    ${this.translateMessage('print')}
                </paper-button>
            `;

            return html`
                ${headHTML}
                <main>
                    ${legalHTML}
                    ${infoHTML}
                    ${achievementHTML}
                    ${signaturesHTML}
                </main>
                ${footerHTML}
                ${printButtonHTML}
                `;

        } else {
            console.warn('No certificate provided');
            return html``
        }
    }

    afterRender() {
        if (this.autoprint) {
            this.print();
        }
    }

    print() {
        let printWindow = window.open(
            "",
            "", "");
        render(
            this.render(),
            printWindow.document.body
        );
        printWindow.document.body.appendChild(this.createQRElement(100));
        printWindow.document.title = this.translateMessage('printTitle');
        printWindow.document.head.appendChild(Cowcert.styleElement(
            printWindow.document,
            printCSS.cssText
        ));
        Cowcert.showPrintDialog(printWindow, true);
    }

    static showPrintDialog(printWindow, closeAfter = true) {
        if(closeAfter) {
            printWindow.addEventListener("afterprint", () => {
                printWindow.close();
            });
        }
        printWindow.print();
    }

    static get translations() {
        return i18n;
    }

    static get languagesAvailable() {
        return Object.keys(this.translations)
    }

    static get styles() {
        return CSS
    }

    static styleElement(document, styles = Cowcert.styles.cssText) {
        let styleElement = document.createElement('style');
        styleElement.innerHTML = styles;
        return styleElement;
    }

    static get properties() {
        return {
            certificate: {
                type: Object,
                value: {},
            },
            autoprint: {
                type: Boolean,
                value: false
            },
            noprint: {
                type: Boolean,
                value: false
            }
        };
    }
}

customElements.define('cow-cert', Cowcert);
