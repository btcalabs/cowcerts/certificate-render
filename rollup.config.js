import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';

export default {
    input: ['src/cowcert.js'],
    output: [
        {
            file: 'dist/cowcert-render.iife.js',
            format: 'iife',
            sourcemap: true
        },
        {
            file: 'dist/cowcert-render.es.js',
            format: 'es',
            sourcemap: true
        },
        {
            file: 'dist/cowcert-render.umd.js',
            format: 'umd',
            sourcemap: true
        }
    ],
    plugins: [
        resolve(),
        babel()
    ]
};
