# TODO
- [ ] Allow importing required WebComponents (`paper-button`)

If we import them, then when the WebComponent is used in 
[another webcomponent][cowcerts-verifier] an error appears:
`dom-module` custom element is already defined. This happens because if both
webcomponents include the same element, the `@polymer/polymer` library is
required twice and tried to register twice, so the custom element registry 
raises an error.

- [ ] ZXing library is not being included in the bundle.

Or yes, but when we use this webcomponent in the [verifier][cowcerts-verifier]
and bundle it, seems that it is not being included. Maybe a verifier bundle
problem.

[cowcerts-verifier]: https://gitlab.com/cowcerts/verifier
