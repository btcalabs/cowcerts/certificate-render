# Cowcerts certificate viewer
This [web component][web-component] renders a digital certificate as specified 
per the [Cowcerts standards for Education][cowcerts-edu] standard.

It uses the Andorran format as the view.

## Usage

Simply include the `cow-cert` element in your `HTML` and use the `certificate` 
attribute to specify the certificate to render.

```html
<cow-cert certificate="{{myCertificate}}"></cow-cert>
```

> You have to include the web components library and the JavaScript module that
> defines the custom element.

> Also you will have to include some scripts to display the print button
> Look at the [demo](demo) folder for more complete examples

### Options
You can specify several options, not just the mandatory `certificate` one.

#### Hide print button
You can hide the print button setting the `noprint` attribute to `true`

```html
<cow-cert noprint certificate="{{myCertificate}}"></cow-cert>
```

#### Autoprint

Using the `autoprint` attribute, the print dialog will automatically appear

```html
<cow-cert autoprint certificate="{{myCertificate}}"></cow-cert>
```


## Development
### Serving the certificate
To serve the certificate, use the `start` npm script:

```shell
npm run start
```

A local server will be available on http://localhost:8081 with the web 
component rendered with a [sample certificate][sample-certificate]

### Automatically refreshing browser on changes
If you are serving the certificate via the `npm run start` command, you can 
also reload automatically the browser if any of the WebComponent sources or demo
files change.

```shell
npm run watch
```

### Build
With [rollup.js][rollup] you can bundle everything in a module, just use the
`build` npm script:

```shell
npm run build
```

> The file [demo/bundled.html](demo/bundled.html) contains a 
> demo for the rendered version (the one compiled by [rollup.js][rollup])

[web-component]: https://www.webcomponents.org/
[eds]: https://europass.cedefop.europa.eu/documents/european-skills-passport/diploma-supplement
[cowcerts]: https://cowcerts.org 
[cowcerts-edu]: https://cowcerts.rtfd.io/en/latest/edu
[sample-certificate]: ./demo/certificate.json
[rollup]: https://rollupjs.org
[livereload]: http://livereload.com/

**\<\> with ♥ by [BTC Assessors](https://btcassessors.com): 
[@davidlj95](https://gitlab.com/davidlj95), [@ccebrecos](https://gitlab.com/ccebrecos)**

[![BTC Assessors](https://i.imgur.com/7nzUvR0.png)](https://www.btcassessors.com)
